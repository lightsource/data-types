<?php

declare(strict_types=1);

namespace LightSource\DataTypes\Tests\unit;

use Codeception\Test\Unit;
use LightSource\DataTypes\DataTypes;

class DataTypesTest extends Unit
{
    public function testCleanStripTagsForStrings()
    {
        $dataTypes = new DataTypes();

        $data = $dataTypes->clean([
            'name' => '<p>Maxim</p>',
        ], [
            'name' => 'string|min:10',
        ]);

        $this->assertEquals([
            'name' => 'Maxim',
        ], $data);
    }

    public function testCleanConvertEntitiesForStrings()
    {
        $dataTypes = new DataTypes();

        $data = $dataTypes->clean([
            'name' => 'Maxim\'s',
        ], [
            'name' => 'string|min:10',
        ]);

        $this->assertEquals([
            'name' => 'Maxim&#039;s',
        ], $data);
    }

    public function testCleanSkipsStripTagsForNumeric()
    {
        $dataTypes = new DataTypes();

        $data = $dataTypes->clean([
            'name' => '<p>Maxim</p>',
        ], [
            'name' => 'numeric|min:10',
        ]);

        $this->assertEquals([
            'name' => '<p>Maxim</p>',
        ], $data);
    }

    public function testCleanSkipsConvertEntitiesForNumeric()
    {
        $dataTypes = new DataTypes();

        $data = $dataTypes->clean([
            'name' => 'Maxim\'s',
        ], [
            'name' => 'numeric|min:10',
        ]);

        $this->assertEquals([
            'name' => 'Maxim\'s',
        ], $data);
    }

    public function testCleanSkipsStripTagsForExcludedStringFields()
    {
        $dataTypes = new DataTypes();

        $data = $dataTypes->clean([
            'name' => '<p>Maxim</p>',
        ], [
            'name' => 'string',
        ], [
            'name',
        ]);

        $this->assertEquals([
            'name' => '<p>Maxim</p>',
        ], $data);
    }

    public function testCleanSkipsConvertEntitiesForExcludedStringFields()
    {
        $dataTypes = new DataTypes();

        $data = $dataTypes->clean([
            'name' => 'Maxim\'s',
        ], [
            'name' => 'string',
        ], [
            'name',
        ]);

        $this->assertEquals([
            'name' => 'Maxim\'s',
        ], $data);
    }

    public function testCleanReturnsOnlyMentionedInRulesFields()
    {
        $dataTypes = new DataTypes();

        $data = $dataTypes->clean([
            'name' => 'Maxim',
            'age' => 20,
        ], [
            'name' => 'string',
        ]);

        $this->assertEquals([
            'name' => 'Maxim',
        ], $data);
    }

    public function testCleanSupportsComplexStringInRules()
    {
        $dataTypes = new DataTypes();

        $data = $dataTypes->clean([
            'name' => 'Maxim',
        ], [
            'name' => 'required|string',
        ]);

        $this->assertEquals([
            'name' => 'Maxim',
        ], $data);
    }

    public function testCleanSupportsArrayInRules()
    {
        $dataTypes = new DataTypes();

        $data = $dataTypes->clean([
            'name' => 'Maxim',
        ], [
            'name' => ['string',],
        ]);

        $this->assertEquals([
            'name' => 'Maxim',
        ], $data);
    }

    public function testCleanAndValidateCallsClean()
    {
        $dataTypes = new DataTypes();

        $data = $dataTypes->cleanAndValidate([
            'name' => 'Maxim\'s',
        ], [
            'name' => 'string|min:10',
        ]);

        $this->assertEquals([
            'name' => 'Maxim&#039;s',
        ], $data->getValidDataAsStrictTypes());
    }

    public function testCleanAndValidateCallsValidate()
    {
        $dataTypes = new DataTypes();

        $data = $dataTypes->cleanAndValidate([
            'name' => 'Maxim\'s',
        ], [
            'name' => 'string|min:20',
        ]);

        $this->assertFalse($data->isValid());
    }
}
