<?php

declare(strict_types=1);

namespace LightSource\DataTypes\Tests\unit;

use Codeception\Test\Unit;
use Illuminate\Support\MessageBag;
use LightSource\DataTypes\DataTypes;
use LightSource\DataTypes\ValidationResult;

class ValidationResultTest extends Unit
{
    public function testGetStrictValidDataForBooleanString()
    {
        $validationResult = new ValidationResult(new MessageBag(), [
            'isActive' => '1',
        ], [
            'isActive' => 'boolean',
        ]);

        $this->assertTrue(true === $validationResult->getValidDataAsStrictTypes()['isActive']);
    }

    public function testGetStrictValidDataForIntString()
    {
        $validationResult = new ValidationResult(new MessageBag(), [
            'age' => 10,
        ], [
            'age' => 'numeric',
        ]);

        $this->assertTrue(10 === $validationResult->getValidDataAsStrictTypes()['age']);
    }

    public function testGetStrictValidDataForFloatString()
    {
        $validationResult = new ValidationResult(new MessageBag(), [
            'age' => '10.5',
        ], [
            'age' => 'numeric',
        ]);

        $this->assertTrue(10.5 === $validationResult->getValidDataAsStrictTypes()['age']);
    }

    public function testGetStrictValidDataForFloatStringWithComma()
    {
        $validationResult = new ValidationResult(new MessageBag(), [
            'age' => '10,5',
        ], [
            'age' => 'numeric',
        ]);

        $this->assertTrue(10.5 === $validationResult->getValidDataAsStrictTypes()['age']);
    }

    public function testGetStrictValidDataSupportsComplexStringInRules()
    {
        $validationResult = new ValidationResult(new MessageBag(), [
            'age' => '10,5',
        ], [
            'age' => 'required|numeric',
        ]);

        $this->assertTrue(10.5 === $validationResult->getValidDataAsStrictTypes()['age']);
    }

    public function testGetStrictValidDataSupportsArrayInRules()
    {
        $validationResult = new ValidationResult(new MessageBag(), [
            'age' => '10,5',
        ], [
            'age' => ['numeric',],
        ]);

        $this->assertTrue(10.5 === $validationResult->getValidDataAsStrictTypes()['age']);
    }
}