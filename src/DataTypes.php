<?php

namespace LightSource\DataTypes;

use LightSource\DataTypes\Interfaces\DataTypesInterface;
use LightSource\DataTypes\Interfaces\ValidationInterface;
use LightSource\DataTypes\Interfaces\ValidationResultInterface;

class DataTypes implements DataTypesInterface
{
    private ValidationInterface $validation;

    public function __construct(?ValidationInterface $validation = null)
    {
        if (!$validation) {
            $validation = new Validation();
        }

        $this->validation = $validation;
    }

    public function setErrorMessages(array $errorMessages): void
    {
        $this->validation->setErrorMessages($errorMessages);
    }

    public function clean(array $data, array $rules, array $excludedStringFields = []): array
    {
        $cleanedData = [];

        foreach ($rules as $fieldName => $fieldRules) {
            if (!isset($data[$fieldName])) {
                continue;
            }

            $isStringType = (is_string($fieldRules) && false !== strpos($fieldRules, 'string')) ||
                (is_array($fieldRules) && in_array('string', $fieldRules, true));

            if (!$isStringType ||
                in_array($fieldName, $excludedStringFields)) {
                $cleanedData[$fieldName] = $data[$fieldName];

                continue;
            }

            $value = $data[$fieldName];

            if (!is_string($value) &&
                !is_numeric($value)) {
                $cleanedData[$fieldName] = $data[$fieldName];
                continue;
            }

            $value = (string)$value;
            $value = strip_tags($value);
            $value = htmlentities($value, ENT_QUOTES);

            $cleanedData[$fieldName] = $value;
        }

        return $cleanedData;
    }

    public function validate(
        array $data,
        array $rules,
        array $customFieldNames = [],
        array $customErrorMessages = []
    ): ValidationResultInterface
    {
        return $this->validation->validate($data, $rules, $customFieldNames, $customErrorMessages);
    }

    public function cleanAndValidate(array $data, array $rules, array $customFieldNames = [], array $customErrorMessages = [], array $excludedStringFieldsFromClean = []): ValidationResultInterface
    {
        $cleanedData = $this->clean($data, $rules, $excludedStringFieldsFromClean);

        return $this->validation->validate($cleanedData, $rules, $customFieldNames, $customErrorMessages);
    }
}
