<?php

declare(strict_types=1);

namespace LightSource\DataTypes\Interfaces;

interface DataTypesInterface extends ValidationInterface
{
    public function clean(array $data, array $rules, array $excludedStringFields = []): array;

    public function cleanAndValidate(
        array $data,
        array $rules,
        array $customFieldNames = [],
        array $customErrorMessages = [],
        array $excludedStringFieldsFromClean = []
    ): ValidationResultInterface;
}
