<?php

declare(strict_types=1);

namespace LightSource\DataTypes\Interfaces;

interface ValidationInterface
{
    public function setErrorMessages(array $errorMessages): void;

    public function validate(
        array $data,
        array $rules,
        array $customFieldNames = [],
        array $customErrorMessages = []
    ): ValidationResultInterface;
}
