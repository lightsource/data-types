<?php

declare(strict_types=1);

namespace LightSource\DataTypes\Interfaces;

use Illuminate\Contracts\Support\MessageBag;

interface ValidationResultInterface
{
    public function getErrors(): MessageBag;

    public function getValidData(): array;

    public function getValidDataAsStrictTypes(): array;

    public function isValid(): bool;
}
