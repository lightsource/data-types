<?php

declare(strict_types=1);

namespace LightSource\DataTypes;

use Illuminate\Contracts\Translation\Translator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;
use LightSource\DataTypes\Interfaces\ValidationInterface;
use LightSource\DataTypes\Interfaces\ValidationResultInterface;
class Validation implements ValidationInterface
{
    private Factory $factory;
    private array $errorMessages;

    public function __construct()
    {
        $this->factory = new Factory($this->getTranslator());
        $this->errorMessages = $this->getErrorMessages();
    }

    protected function getTranslator(): Translator
    {
        return new class implements Translator {

            public function get($key, array $replace = [], $locale = null)
            {
                return '';
            }

            public function choice($key, $number, array $replace = [], $locale = null)
            {
                return '';
            }

            public function getLocale()
            {
                return 'en';
            }

            public function setLocale($locale)
            {

            }
        };
    }

    protected function getErrorMessages(): array
    {
        return [
            'accepted' => 'The :attribute must be accepted.',
            'active_url' => 'The :attribute is not a valid URL.',
            'after' => 'The :attribute must be a date after :date.',
            'alpha' => 'The :attribute may only contain letters.',
            'alpha_dash' => 'The :attribute may only contain letters, numbers, and dashes.',
            'alpha_num' => 'The :attribute may only contain letters and numbers.',
            'array' => 'The :attribute must be an array.',
            'before' => 'The :attribute must be a date before :date.',
            'between' => 'The :attribute must be between :min and :max.',
            'boolean' => 'The :attribute field must be true or false.',
            'confirmed' => 'The :attribute confirmation does not match.',
            'date' => 'The :attribute is not a valid date.',
            'date_format' => 'The :attribute does not match the format :format.',
            'different' => 'The :attribute and :other must be different.',
            'digits' => 'The :attribute must be :digits digits.',
            'digits_between' => 'The :attribute must be between :min and :max digits.',
            'email' => 'The :attribute must be a valid email address.',
            'exists' => 'The selected :attribute is invalid.',
            'filled' => 'The :attribute field is required.',
            'image' => 'The :attribute must be an image.',
            'in' => 'The selected :attribute is invalid.',
            'integer' => 'The :attribute must be an integer.',
            'ip' => 'The :attribute must be a valid IP address.',
            'json' => 'The :attribute must be a valid JSON string.',
            'max' => 'The :attribute may not be greater than :max.',
            'mimes' => 'The :attribute must be a file of type: :values.',
            'min' => 'The :attribute must be at least :min.',
            'not_in' => 'The selected :attribute is invalid.',
            'numeric' => 'The :attribute must be a number.',
            'present' => 'The :attribute field must be present.',
            'regex' => 'The :attribute format is invalid.',
            'required' => 'The :attribute field is required.',
            'required_if' => 'The :attribute field is required when :other is :value.',
            'required_unless' => 'The :attribute field is required unless :other is in :values.',
            'required_with' => 'The :attribute field is required when :values is present.',
            'required_with_all' => 'The :attribute field is required when :values is present.',
            'required_without' => 'The :attribute field is required when :values is not present.',
            'required_without_all' => 'The :attribute field is required when none of :values are present.',
            'same' => 'The :attribute and :other must match.',
            'size' => 'The :attribute must be :size.',
            'string' => 'The :attribute must be a string.',
            'timezone' => 'The :attribute must be a valid zone.',
            'unique' => 'The :attribute has already been taken.',
            'url' => 'The :attribute format is invalid.'
        ];
    }

    public function setErrorMessages(array $errorMessages): void
    {
        $this->errorMessages = array_merge($this->errorMessages, $errorMessages);
    }

    public function validate(
        array $data,
        array $rules,
        array $customFieldNames = [],
        array $customErrorMessages = []
    ): ValidationResultInterface
    {
        $errorMessages = array_merge($this->errorMessages, $customErrorMessages);

        $validator = $this->factory->make($data, $rules, $errorMessages, $customFieldNames);

        $validData = [];

        try {
            $validData = $validator->validate();
        } catch (ValidationException $ex) {

        }

        return new ValidationResult($validator->errors(), $validData, $rules);
    }
}
