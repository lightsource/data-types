<?php

declare(strict_types=1);

namespace LightSource\DataTypes;

use Illuminate\Contracts\Support\MessageBag;
use LightSource\DataTypes\Interfaces\ValidationResultInterface;

class ValidationResult implements ValidationResultInterface
{
    private MessageBag $errors;
    private array $validData;
    private array $validationRules;

    public function __construct(MessageBag $errors, array $validData, array $validationRules)
    {
        $this->errors = $errors;
        $this->validData = $validData;
        $this->validationRules = $validationRules;
    }

    protected function getStrictNumeric($value)
    {
        if (!is_string($value)) {
            return $value;
        }

        if (false !== strpos($value, '.') ||
            false !== strpos($value, ',')) {
            $value = str_replace(',', '.', $value);
            $value = (float)$value;
        } else {
            $value = (int)$value;
        }

        return $value;
    }

    public function getErrors(): MessageBag
    {
        return $this->errors;
    }

    public function getValidData(): array
    {
        return $this->validData;
    }

    public function getValidDataAsStrictTypes(): array
    {
        $validData = $this->validData;

        $isPresentWithType = function ($type, $fieldName, $fieldRules) use (&$validData) {
            if (!isset($validData[$fieldName])) {
                return false;
            }
            return (is_string($fieldRules) && false !== strpos($fieldRules, $type)) ||
                (is_array($fieldRules) && in_array($type, $fieldRules, true));
        };

        foreach ($this->validationRules as $fieldName => $fieldRules) {
            if ($isPresentWithType('boolean', $fieldName, $fieldRules)) {
                $validData[$fieldName] = (bool)$validData[$fieldName];
                continue;
            }

            if ($isPresentWithType('numeric', $fieldName, $fieldRules)) {
                $validData[$fieldName] = $this->getStrictNumeric($validData[$fieldName]);
                continue;
            }
        }
        return $validData;
    }

    public function isValid(): bool
    {
        return $this->errors->isEmpty();
    }
}
