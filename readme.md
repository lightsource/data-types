## 1. What is it

Wrapper above [Illuminate/validation](https://packagist.org/packages/illuminate/validation) from Laravel.

Makes using it outside of Laravel easier and adds a 'clean' functionality.

List of available rules see in the [Laravel Docs](https://laravel.com/docs/11.x/validation).

## 2. Installation

```
composer require lightsource/data-types
```

## 3. Example of usage

```php
use LightSource\DataTypes\DataTypes;

require_once __DIR__ . '/vendor/autoload.php';

$dataTypes = new DataTypes();

// can be $_POST
$data = [
 'name' => 'Their\'s',
];

// your rules, see Laravel Docs for reference
$rules = [
  'name' => 'string|required',
];

// optionally, to strip tags and 
// apply 'htmlentities' for existing string arguments
$data = $dataTypes->clean($data, $rules);

// returns ValidationResultInterface
$validationResult = $dataTypes->validate($data, $rules);
```

```php
interface ValidationResultInterface
{
    // see how to work with errors in the Laravel Docs
    public function getErrors(): MessageBag;

    public function getValidData(): array;

    public function isValid(): bool;
}
```

## 4. Customization

### 4.1) Custom fields names (in error messages)

By default, in error messages array keys are used as field names, with uppercase of the first letter.   

So, 'name' will be turned into 'Name'.

If you want to add 'aliases', that must be used in the errors instead of array keys, use it:

```php
$dataTypes = new DataTypes();

$aliases = [
 'price' => 'Total price',
];

$dataTypes->validate($data, $rules, $aliases);
```

Then, if e.g. the min validation fails, you'll receive "**The Total price must be at least x.**" instead of "**The Price must be at least x.**"

### 4.2) Custom error messages (global)

The package has built-in error messages. See `Validation.php getErrorMessages()` for reference. 

If you want to override them, use it:

```php
$dataTypes = new DataTypes();

$myErrors = [
 'required' => 'The :attribute field is required.', 
 // ...
];

$dataTypes->setErrorMessages($myErrors);
```

### 4.3) Custom error messages (for one validation)

To override only for one validation, use it:

```php
$dataTypes = new DataTypes();

$myErrors = [
 'required' => 'The :attribute field is required.', 
 // ...
];

$dataTypes->validate($data, $rules, [], $myErrors);
```